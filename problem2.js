const fs = require('fs');
const path = require('path');

module.exports = function problem2() {

    //reading lipsum file and converting it's data to uppercase

    fs.readFile('Data/lipsum.txt', 'utf-8', (err, data) => {

        if (err) {
            console.error("Cannot read the file");
        } else {

            console.log(data);
            const dataInUpperCase = data.toUpperCase();

            //writing uppercase data to dataInUpperCase.txt file

            fs.writeFile('dataInUpperCase.txt', dataInUpperCase, (err) => {

                if (err) {
                    console.error("Cannot input the data");
                } else {

                    //writing file name to filenames.txt file

                    fs.writeFile('filenames.txt', 'dataInUpperCase.txt\n', (err) => {

                        if (err) {
                            console.error("Cannot input the filename");
                        } else {

                            //reading file data of dataInUpperCase

                            fs.readFile('dataInUpperCase.txt', 'utf-8', (err, dataOfUpperCase) => {
                                
                                if (err) {
                                    console.error("Cannot read the file");
                                } else {

                                    console.log(dataOfUpperCase);
                                    const dataInLowerCase = dataOfUpperCase.toLowerCase();

                                    //writing lowerCase data to dataInLowerCase.txt file

                                    fs.writeFile('dataInLowerCase.txt', dataInLowerCase, (err) => {

                                        if (err) {
                                            console.log(err);
                                        } else {

                                            //appending the filenames.txt with dataInLowerCase.txt name

                                            fs.appendFile('filenames.txt', 'dataInLowerCase.txt\n', (err) => {

                                                if (err) {
                                                    console.log(err);
                                                } else {

                                                    //reading file from dataInUpperCase and dataInLowerCase

                                                    fs.readFile('dataInLowerCase.txt', 'utf-8', (err, dataOfLowerCase) => {

                                                        if (err) {
                                                            console.error(err);
                                                        } else {

                                                            console.log(dataOfUpperCase);
                                                            console.log(dataOfLowerCase);
                                                            const merge = (dataOfUpperCase.toString() + dataOfLowerCase.toString()).split('.');
                                                            merge.sort();

                                                            //writing sorted and merged data to merge.txt

                                                            fs.writeFile('merge.txt', merge, (err) => {

                                                                if (err) {
                                                                    console.error(err);
                                                                } else {

                                                                    //appending filenames.txt with merge.

                                                                    fs.appendFile('filenames.txt', 'merge.txt', (err) => {

                                                                        if (err) {
                                                                            console.log(error);
                                                                        } else {

                                                                            //read filename.txt and delete it's content simultaneously

                                                                            fs.readFile('filenames.txt', 'utf-8', (err, filename) => {

                                                                                const fileArray = filename.split('\n');

                                                                                for (let iterate = 0; iterate < fileArray.length; iterate++){ 

                                                                                    console.log(fileArray[iterate]);

                                                                                    fs.unlink(`./${fileArray[iterate]}`, (err) => {
                                                                                        if (err) {
                                                                                            console.error("Cannot delete the files");
                                                                                        }
                                                                                    });
                                                                                }
                                                                            });
                                                                        }
                                                                    });
                                                                }
                                                            });
                                                        }
                                                    });
                                                }
                                            });
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            });
        }
    });
}
