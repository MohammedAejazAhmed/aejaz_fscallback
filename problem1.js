const fs = require('fs');
const path = require('path');

function makeDir(folderName) {

    //Creating a directory    

    fs.mkdir(path.join(__dirname, folderName), (err) => {

        if (err) {
            console.error(err);
        }

        //Creating .json files in the directory      

        for (let iterate = 0; iterate < 5; iterate++) {

            fs.writeFile(`./${folderName}/${iterate}.json`, "tota", (err) => {

                if (err) {
                    console.log("cannot create the file");
                }
            });
        }

        //Deleting the files created        

        for (let iterate = 0; iterate < 5; iterate++) {

            fs.unlink(`./${folderName}/${iterate}.json`, (err) => {

                if (err) {
                    console.error("Cannot delete the files");
                }
            });
        }
    });
}

module.exports = makeDir;
